SELECT *
FROM employee
WHERE age > 30;

CREATE VIEW employee_view AS
SELECT *
FROM employee
WHERE age > 30;



CREATE VIEW product_view AS
SELECT *
FROM product
WHERE price > 3000;