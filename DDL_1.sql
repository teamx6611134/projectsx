CREATE TABLE customers
( customer_id number(10) NOT NULL,
  customer_name varchar2(50) NOT NULL,
  city varchar2(50),
  CONSTRAINT customers_pk PRIMARY KEY (customer_id)
);

CREATE TABLE employees
(
    employee_id number(10) NOT NULL,
    first_name varchar2(50) NOT NULL,
    last_name varchar2(50) NOT NULL,
    mobile_number varchar2(20),
    country varchar2(50),
    location varchar2(50),
    other_details varchar2(100),
    CONSTRAINT employees_pk PRIMARY KEY (employee_id)
);

create table products
(
    product_id number(10) not null,
    product_name varchar2(50) not null,
    product_description varchar2(100),
    product_price number(10,2),
    CONSTRAINT products_pk PRIMARY KEY (product_id)
);

create table products_histories
(
    id number(10) not null,
	product_id number(10) not null,
    product_name varchar2(50) not null,
    product_description varchar2(100),
    product_price number(10,2),
    CONSTRAINT products_pk PRIMARY KEY (product_id)
);

/// giri prasad changes

create table orders(
    order_id number(10) not null,
    order_date date not null,
    customer_id number(10) not null,
    employee_id number(10) not null,
    CONSTRAINT orders_pk PRIMARY KEY (order_id),
    CONSTRAINT orders_fk1 FOREIGN KEY (customer_id) REFERENCES customers(customer_id),
    CONSTRAINT orders_fk2 FOREIGN KEY (employee_id) REFERENCES employees(employee_id)
create table test
(
    id number(10) not null,
	product_id number(10) not null,
    product_name varchar2(50) not null,
    product_description varchar2(100),
    product_price number(10,2),
    CONSTRAINT products_pk PRIMARY KEY (product_id)
);