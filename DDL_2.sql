ALTER TABLE customers
  ADD country varchar2(45);

ALTER TABLE employees
  ADD country varchar2(45);

ALTER TABLE product
  add pro_type varchar2(20);
  
  alter table product
  add pro_price number(10,2);