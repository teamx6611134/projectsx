CREATE PROCEDURE CheckConcatInsert
    @param1 VARCHAR(50),
    @param2 VARCHAR(50)
AS
BEGIN
    -- Check if record exists
    IF EXISTS (SELECT * FROM employee WHERE age = @param1)
    BEGIN
        -- Concatenate values
        DECLARE @concatenatedValue VARCHAR(100)
        SET @concatenatedValue = @param1 + @param2

        -- Insert into table
        INSERT INTO YourTable (Column1, Column2) VALUES (@param1, @concatenatedValue)
    END
END